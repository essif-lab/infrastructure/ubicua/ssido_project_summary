Project plan of SSIDO
=====================

**Components**

The SSIDO project includes a development and testing of the following open-source components:
- [JSSI CryptoLib](https://gitlab.grnet.gr/essif-lab/infrastructure/ubicua/deliverables/-/blob/master/functional_specification_of_cryptolib_component.md), a Java cryptographic library.
- [JSSI Wallet](https://gitlab.grnet.gr/essif-lab/infrastructure/ubicua/deliverables/-/blob/master/functional_specification_of_wallet_component.md), an SSI Wallet implementation in Java.
- [SSIDO Agents](https://gitlab.grnet.gr/essif-lab/infrastructure/ubicua/deliverables/-/blob/master/functional_specification_of_agents_component.md), a Java implementation of Authenticator and Validator.

The software deliverables are based on components located at [the Ubicua GitHub repositories](https://github.com/UBICUA-JSSI). 


**Scheduling**

A draft schedule of development tasks is presented in the table below:

<table>
<tr><th>Component</th><th>Begin date</th><th>End date</th><th>Tasks</th><th>Readiness</th></tr>
<tr><td>JSSI CryptoLib</td><td>01.11.2020</td><td>31.12.2020</td><td>Code adaptation, unit tests</td><td>Done</td></tr>
<tr><td>JSSI Wallet</td><td>01.11.2020</td><td>31.12.2020</td><td>Code adaptation, unit tests</td><td>Done</td></tr>
<tr><td>SSIDO Agents</td><td>01.01.2021</td><td>16.06.2021</td><td>Code development, unit tests, prototype and demo case*</td><td>Done</td></tr>
</table>

(*) A SSIDO-enabled demo web application will be developed as an integral test case.


**Milestones**

Interoperability issues with other eSSIF-Lab IOC and BOC projects will be considered.






